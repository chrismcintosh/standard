//Call The Package Dependencies
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var beep = require('beepbeep');

var path = {
  ALL: ['./assets/scss/*.scss', './assets/scss/**/*.scss'],
  SASS_SRC: ['./assets/scss/*.scss', './assets/scss/**/*.scss'],
  SASS_BUILD: './'
};

//Sass Options
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

gulp.task('sass', function () {
  return gulp
    .src(path.SASS_SRC)
    .pipe(plumber({
         errorHandler: function (err) {
              beep(2);
              console.log(err);
              this.emit('end');
           }
    }))
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.SASS_BUILD));
});

// Run to create production ready assets
gulp.task('production', function() {
     return gulp
       .src(path.SASS_SRC)
       .pipe(plumber({
        errorHandler: function (err) {
            beep(2);
            console.log(err);
            this.emit('end');
          }
        }))
       .pipe(sass(sassOptions))
       .pipe(gulp.dest(path.SASS_BUILD));
});

// Watch
gulp.task('watch', function() {
  gulp.watch(path.SASS_SRC, ['sass'])
  .on('change', function(event) {
   console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
})

gulp.task('default', ['sass', 'watch']);
